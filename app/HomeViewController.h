//
//  HomeViewController.h
//  app
//
//  Created by 4D_Koki on 15/8/2017.
//  Copyright © 2017 koki.io. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@end
