//
//  ShipFromHomeViewController.h
//  app
//
//  Created by 4D_Koki on 15/8/2017.
//  Copyright © 2017 koki.io. All rights reserved.
//

#import "BaseViewController.h"
#import <UIKit/UIKit.h>

@interface ShipFromHomeViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *addressIcon;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;

@property (weak, nonatomic) IBOutlet UIImageView *nameIcon;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UIImageView *mobileIcon;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;

@property (weak, nonatomic) IBOutlet UIButton *bookNowButton;

@end
