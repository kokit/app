//
//  AppDelegate.m
//  app
//
//  Created by 4D_Koki on 15/8/2017.
//  Copyright © 2017 koki.io. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "TrackingViewController.h"
#import "MoreViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //Load starting settings
    [self loadStart];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)loadStart{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    self.window.rootViewController = [self tabbarViewController];
    
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    [self.window makeKeyAndVisible];
}

- (UITabBarController *)tabbarViewController{    
    UITabBarController *tab = [UITabBarController new];
    self.window.rootViewController = tab;
    tab.tabBar.translucent = NO;
    
    //创建相应的子控制器
//    HomeViewController *vc1 = [[HomeViewController alloc] init];
    HomeViewController *vc1 = [[HomeViewController alloc] init];
    UINavigationController *vc11 = [[UINavigationController alloc] initWithRootViewController:vc1];
    vc1.view.backgroundColor = [UIColor whiteColor];
    vc11.tabBarItem.title = @"Service";
//    vc11.tabBarItem.image = [[UIImage imageNamed:@"Home_normal"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    vc11.tabBarItem.selectedImage = [[UIImage imageNamed:@"Home_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    TrackingViewController *vc2 = [[TrackingViewController alloc]init];
    UINavigationController *vc22 = [[UINavigationController alloc] initWithRootViewController:vc2];
    vc22.tabBarItem.title = @"Tracking";
//    vc22.tabBarItem.image = [[UIImage imageNamed:@"List_normal"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    vc22.tabBarItem.selectedImage = [[UIImage imageNamed:@"List_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    MoreViewController *vc3 = [[MoreViewController alloc]init];
    UINavigationController *vc33 = [[UINavigationController alloc] initWithRootViewController:vc3];
    vc3.view.backgroundColor = [UIColor yellowColor];
    vc33.tabBarItem.title = @"More";
    //    vc33.tabBarItem.image = [[UIImage imageNamed:@"List_normal"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //    vc33.tabBarItem.selectedImage = [[UIImage imageNamed:@"List_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

    [[UITabBar appearance] setBarTintColor:[UIColor orangeColor]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    tab.viewControllers = @[vc11,vc22,vc33];
    
    return tab;
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"app"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
