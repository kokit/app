//
//  MoreViewController.m
//  app
//
//  Created by 4D_Koki on 15/8/2017.
//  Copyright © 2017 koki.io. All rights reserved.
//

#import "MoreViewController.h"

@interface MoreViewController ()

@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setupUI{
    [self setNavigationBarHidden:NO animated:YES];
    [self setNavigationBarTranslucent:NO];
    [self setupNavigationBar];
    [self setNavigationBarBaselineHidden:NO];
    [self setNavigationBarTitle:NavigationBarTitleTypeMore];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
