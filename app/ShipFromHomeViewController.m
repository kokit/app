//
//  ShipFromHomeViewController.m
//  app
//
//  Created by 4D_Koki on 15/8/2017.
//  Copyright © 2017 koki.io. All rights reserved.
//

#import "ShipFromHomeViewController.h"

@interface ShipFromHomeViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomConstraint;
@end

@implementation ShipFromHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupUI{
    //bar buttons
    [self cleanUpNavigationBar];
    [self addNavigationBarButtonType:NavigationBarButtonTypeBack onRight:NO];
    [self setupNavigationBar];
    
    //nav bar
    [self setNavigationBarHidden:NO animated:YES];
    [self setNavigationBarTranslucent:NO];
    [self setNavigationBarBaselineHidden:NO];
    [self setNavigationBarTitle:NavigationBarTitleTypeShipFromHome];
    [self setNavigationBarBackgroundColor:[UIColor orangeColor]];
    [self setNavigationBarTitleColor:[UIColor whiteColor]];
    
    //xib layout
    self.bookNowButton.layer.cornerRadius = self.bookNowButton.frame.size.height/2;
    self.bookNowButton.layer.masksToBounds = YES;
    
    //tap gesture
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:tap];
}

- (void)viewTapped:(id)sender{
    [self.nameTextField resignFirstResponder];
    [self.mobileTextField resignFirstResponder];
    [self.addressTextField resignFirstResponder];
}


#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.scrollViewBottomConstraint.constant = keyboardSize.height;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        self.scrollViewBottomConstraint.constant = 0;
    }];
}

@end
