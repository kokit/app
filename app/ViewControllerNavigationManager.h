//
//  ViewControllerNavigationManager.h
//  goodetalk
//
//  Created by SteveLai on 2/3/2017.
//  Copyright © 2017 FOUT DIRECTIONS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import "BaseViewControlerEventHandler.h"

typedef enum {
    BaseViewControllerHome,
    BaseViewControllerBack,
    BaseViewControllerLeftMenu,
    
    NavigationEventShipFromHome,
}ViewControllerNavigationEvent;

@class UIViewController;
@protocol ViewControllerInteractionDelegate <NSObject>
@optional
-(void)onViewController:(UIViewController *)vc interaction:(ViewControllerNavigationEvent)event data:(id)object;
@end

@interface ViewControllerNavigationManager : NSObject<ViewControllerInteractionDelegate>
+(instancetype)sharedManager;
@property (nonatomic)UIViewController *rootViewController;
@end
