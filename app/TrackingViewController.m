//
//  TrackingViewController.m
//  app
//
//  Created by 4D_Koki on 15/8/2017.
//  Copyright © 2017 koki.io. All rights reserved.
//

#import "TrackingViewController.h"

@interface TrackingViewController ()
@property (weak, nonatomic) IBOutlet UIButton *trackingButton;

@end

@implementation TrackingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupUI{
    [self setNavigationBarHidden:NO animated:YES];
    [self setNavigationBarTranslucent:NO];
    [self setupNavigationBar];
    [self setNavigationBarBaselineHidden:NO];
    [self setNavigationBarTitle:NavigationBarTitleTypeTracking];
    [self setNavigationBarBackgroundColor:[UIColor orangeColor]];
    [self setNavigationBarTitleColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    self.trackingButton.layer.borderColor = [UIColor orangeColor].CGColor;
    self.trackingButton.layer.borderWidth = 1;
    self.trackingButton.layer.cornerRadius = 12.0f;
    self.trackingButton.layer.masksToBounds = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
