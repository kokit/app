//
//  ViewControllerNavigationManager.m
//  goodetalk
//
//  Created by SteveLai on 2/3/2017.
//  Copyright © 2017 FOUT DIRECTIONS. All rights reserved.
//

#import "ViewControllerNavigationManager.h"
#import "BaseViewController.h"
#import "HomeViewController.h"
#import "ShipFromHomeViewController.h"

@interface ViewControllerNavigationManager ()

@end

@implementation ViewControllerNavigationManager

+ (instancetype)sharedManager {
    static ViewControllerNavigationManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


-(void)onViewController:(UIViewController *)vc interaction:(ViewControllerNavigationEvent)event data:(id)object
{
    switch (event) {
//        case ViewControllerOnDemo:{
//            DemoViewController  *demoVC = [[DemoViewController alloc] init];
//            [vc.navigationController pushViewController:demoVC animated:YES];
//            break;
//        }
            
        case NavigationEventShipFromHome:{
            ShipFromHomeViewController *shipVc = [[ShipFromHomeViewController alloc] init];
            shipVc.hidesBottomBarWhenPushed = YES;
            [vc.navigationController pushViewController:shipVc animated:YES];
        }
            
        default:
            break;
    }
//    if ([vc kindOfClass:[UIViewController class]]) {
//        
//    }
}

-(void)setRootViewController:(UIViewController *)rootViewController
{
    if ([rootViewController isKindOfClass:[BaseViewController class]]){
//        Do setup here
        BaseViewController *baseVC = (BaseViewController *)rootViewController;
        baseVC.interactionDelegate = [ViewControllerNavigationManager sharedManager];
        self.rootViewController = baseVC;
    }
}
@end
