//
//  HomeViewController.m
//  app
//
//  Created by 4D_Koki on 15/8/2017.
//  Copyright © 2017 koki.io. All rights reserved.
//

#import "HomeViewController.h"
#import "ServiceTableViewCell.h"

@interface HomeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ServiceTableViewCell class]) bundle:nil]
            forCellReuseIdentifier:[ServiceTableViewCell cellIdentifier]];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupUI{
    [self setNavigationBarHidden:NO animated:YES];
    [self setNavigationBarTranslucent:NO];
    [self setupNavigationBar];
    [self setNavigationBarBaselineHidden:NO];
    [self setNavigationBarTitle:NavigationBarTitleTypeService];
    [self setNavigationBarBackgroundColor:[UIColor orangeColor]];
    [self setNavigationBarTitleColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
}

#pragma mark - TableView Datasource & Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ServiceTableViewCell * cell =  [tableView dequeueReusableCellWithIdentifier:[ServiceTableViewCell cellIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        [self.interactionDelegate onViewController:self interaction:NavigationEventShipFromHome data:nil];
    } else if(indexPath.row == 1){
        
    }
}

@end
