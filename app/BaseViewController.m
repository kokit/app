//
//  BaseViewController.m
//  goodetalk
//
//  Created by SteveLai on 2/3/2017.
//  Copyright © 2017 FOUT DIRECTIONS. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()
@property (nonatomic) NSMutableArray *leftBarButtons;
@property (nonatomic) NSMutableArray *rightBarButtons;
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
- (void)cleanUpNavigationBar{
    _rightBarButtons = [NSMutableArray array];
    _leftBarButtons = [NSMutableArray array];
}

- (void)addNavigationBarButtonType:(NavigationBarButtonType)type onRight:(BOOL)right
{
    UIView *temp;
    switch (type) {
        case NavigationBarButtonTypeBack:{
            UIButton *barBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
            [barBtn setTitle:@"<" forState:UIControlStateNormal];
            [barBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [barBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
            [barBtn addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
            [barBtn setFrame:CGRectMake(0, 0, 20, 20)]; //Can be (0, 0, 40, 20) for larger click space.
            [barBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            temp = barBtn;
            break;
        }
        case NavigationBarButtonTypeBurger:{
            UIButton *barBtn =  [UIButton buttonWithType:UIButtonTypeSystem];
            [barBtn setTitle:@"burger" forState:UIControlStateNormal];
            [barBtn addTarget:self action:@selector(onBurger) forControlEvents:UIControlEventTouchUpInside];
            [barBtn sizeToFit];
//            [barBtn setFrame:CGRectMake(0, 0, 40, 20)]; //Can be (0, 0, 40, 20) for larger click space.
            [barBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            temp = barBtn;
            break;
        }
//        case NavigationBarButtonTypeSave:{
//            UIButton *barBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
//            [barBtn setFrame:CGRectMake(0, 0, 100, 40)];
//            NSString *save = PIOLocalizedString(@"save", nil);
//            [barBtn setTitle:save forState:UIControlStateNormal];
//            [barBtn setTitleColor:UICOLOR_NAVIGATION_BACK forState:UIControlStateNormal];
//            [barBtn.titleLabel setFont:[UIFont systemFontOfSize:17]];
//            [barBtn addTarget:self action:@selector(onSave) forControlEvents:UIControlEventTouchUpInside];
//            [barBtn sizeToFit];
//            temp = barBtn;
//            break;
//        }
//            
//        case NavigationBarButtonTypeLoading:{
//            UIActivityIndicatorView *barIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//            [barIndicator startAnimating];
//            temp = barIndicator;
//            break;
//        }
            
        default:{
            break;
        }
    }
    
    if (right){
        [self.rightBarButtons addObject:temp];
    }else{
        [self.leftBarButtons addObject:temp];
    }
}

- (void)setupNavigationBar
{
    // 1. Right
    CGFloat rX = 0.0f;
    CGFloat rTotalWidth = 0.0f;
    CGFloat rMaxHeight = 0.0f;
    for (UIView *view in self.rightBarButtons) {
        rTotalWidth += (16.0f + view.frame.size.width);
        if (view.frame.size.height > rMaxHeight) {
            rMaxHeight = view.frame.size.height;
        }
    }
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rTotalWidth - 16.0f, rMaxHeight)];
    for (UIView *view in self.rightBarButtons) {
        view.frame = CGRectMake(rX, (rMaxHeight - view.frame.size.height)/2, view.frame.size.width, view.frame.size.height);
        [rightBarButtonItems addSubview:view];
        rX += (16.0f + view.frame.size.width);
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    
    // 2. Left
    CGFloat lX = 0.0f;
    CGFloat lTotalWidth = 0.0f;
    CGFloat lMaxHeight = 0.0f;
    for (UIView *view in self.leftBarButtons) {
        lTotalWidth += (16.0f + view.frame.size.width);
        if (view.frame.size.height > lMaxHeight) {
            lMaxHeight = view.frame.size.height;
        }
    }
    
    UIView *leftBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, lTotalWidth - 16.0f, lMaxHeight)];
    for (UIView *view in self.leftBarButtons) {
        view.frame = CGRectMake(lX, (lMaxHeight - view.frame.size.height)/2, view.frame.size.width, view.frame.size.height);
        [leftBarButtonItems addSubview:view];
        lX += (16.0f + view.frame.size.width);
    }
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarButtonItems];
}

- (void)setNavigationBarTranslucent:(BOOL)translucent{
    self.navigationController.navigationBar.translucent = translucent;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated
{
    if (hidden){
        [self.navigationController setNavigationBarHidden:hidden animated:animated];
    } else{
        if (animated){
            self.navigationController.navigationBarHidden = NO;
            self.navigationController.navigationBar.alpha = 0.99f;
            [UIView animateWithDuration:0.2f animations:^{
                self.navigationController.navigationBar.alpha = 1.0f;
            } completion:^(BOOL finished) {
                self.navigationController.navigationBarHidden = NO;
            }];
        }else{
            [self.navigationController setNavigationBarHidden:hidden];
        }
        
    }
}

- (void)setNavigationBarTitle:(NavigationBarTitleType)type{
    NSString *title = @"";
    switch (type) {
        case NavigationBarTitleTypeService:{
            title = @"Pick a service";
            break;
        }
            
        case NavigationBarTitleTypeTracking:{
            title = @"Tracking";
            break;
        }
            
        case NavigationBarTitleTypeMore:{
            title = @"More";
            break;
        }
            
        case NavigationBarTitleTypeShipFromHome:{
            title = @"Ship from Home";
            break;
        }
            
        case NavigationBarTitleTypeDefault:
        default:
            title = @"";
            break;
    }
    //This only set the navigation bar
    //If want to set with UITabbarItem at the same time
    //then use
    //self.title = title
    [self.navigationItem setTitle:title];
}

- (void)setNavigationBarBaselineHidden:(BOOL)hidden{
    if (hidden) {
        [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    } else{
        [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setShadowImage:nil];
    }
}

- (void)setNavigationBarBackgroundColor:(UIColor *)color{
    self.navigationController.navigationBar.barTintColor = color;
}

- (void)setNavigationBarTitleColor:(UIColor *)color{
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName, nil]];
}

#pragma mark - Properties
- (id<ViewControllerInteractionDelegate>)interactionDelegate{
    return [ViewControllerNavigationManager sharedManager];
}

- (NSMutableArray *)leftBarButtons
{
    if (!_leftBarButtons) {
        _leftBarButtons = [NSMutableArray array];
    }
    return _leftBarButtons;
}

- (NSMutableArray *)rightBarButtons
{
    if (!_rightBarButtons) {
        _rightBarButtons = [NSMutableArray array];
    }
    return _rightBarButtons;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
//    return UIStatusBarStyleDefault; //Black status bar text
    return UIStatusBarStyleLightContent; //White status bar text
}

#pragma mark - Actions
- (void)addTapGestureToView:(UIView *) view onSelection:(nullable SEL)selector from:(id)target{
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:selector];
    tapGestureRecognizer.numberOfTapsRequired    = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    tapGestureRecognizer.delegate                = target;
    [view addGestureRecognizer:tapGestureRecognizer];
}

- (void)onBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onBurger{
    
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
//{
//    return NO;
//}

@end
