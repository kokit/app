//
//  ServiceTableViewCell.h
//  app
//
//  Created by 4D_Koki on 15/8/2017.
//  Copyright © 2017 koki.io. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
+ (NSString *)cellIdentifier;
@end
