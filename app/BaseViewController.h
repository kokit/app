//
//  BaseViewController.h
//  goodetalk
//
//  Created by SteveLai on 2/3/2017.
//  Copyright © 2017 FOUT DIRECTIONS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewControllerNavigationManager.h"
//#import "AppAPIManager.h"
//#import "NetworkManager.h"

@interface BaseViewController : UIViewController<UIGestureRecognizerDelegate>

typedef enum {
    NavigationBarButtonTypeBack,
    NavigationBarButtonTypeBurger,
}NavigationBarButtonType;

typedef enum {
    NavigationBarTitleTypeDefault, //Default is @"";
    
    NavigationBarTitleTypeService, //@"Service"
    NavigationBarTitleTypeShipFromHome, //@"Ship from home"
    
    NavigationBarTitleTypeTracking, //@"Tracking"
    NavigationBarTitleTypeMore, //@"More"
}NavigationBarTitleType;

@property (nonatomic, weak) _Null_unspecified id <ViewControllerInteractionDelegate> interactionDelegate;

-(void)cleanUpNavigationBar;
-(void)addNavigationBarButtonType:(NavigationBarButtonType)type onRight:(BOOL)right;
-(void)setupNavigationBar;
-(void)setNavigationBarTranslucent:(BOOL)translucent;
-(void)setNavigationBarTitle:(NavigationBarTitleType)type;
-(void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated;
-(void)setNavigationBarBaselineHidden:(BOOL)hidden;
-(void)setNavigationBarBackgroundColor:(UIColor *_Nonnull)color;
-(void)setNavigationBarTitleColor:(UIColor *_Nonnull)color;
-(void)addTapGestureToView:(UIView *_Null_unspecified) view onSelection:(nullable SEL)selector from:(id _Nonnull )target;

- (void)onBurger;
@end
